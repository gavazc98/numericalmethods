
== Numerical Methods for Engineering. Week 3-1 practice

== Objectives

* *To reinforce the concepts presented in class by implementing them in
Python.*
* *To familiarise with basic Python instructions and data structures.*
* *To get used to Jupyter Notebooks as a tool to experiment with numeric
algorithms.*

Let us start by importing some useful libraries.


+*In[1]:*+
[source, ipython3]
----
import string
import math
----

== 1. Floating point

To get us started, let’s experiment with floating point, and round off
errors. In class, we saw that in certain cases, repeating the same
operation on FPs could lead to unexpected results. As in the following
function:


+*In[2]:*+
[source, ipython3]
----
def fp_test(x):
    if x <= 1/2:
        return 2 * x
    if x > 1/2:
        return 2*x - 1
----

Create a routine that calls the previous function 100 times with x =
1/10


+*In[3]:*+
[source, ipython3]
----
# Complete funciton here
def fp():
    x = 1/10
    for i in range(100):
        x = fp_test(x)
        print (fp_test(x))
    

   

----


+*In[ ]:*+
[source, ipython3]
----
fp()
----

== 2. Iterative method to approximate exp function

Implement an iterative routine to approximate exp function, by
implementing its series expansion, as follows:

[latexmath]
++++
\[e^{x} = \sum^{\infty}_{n=0} \frac{x^n}{n!} = 1 + x + \frac{x^2}{2!} + \frac{x^3}{3!} + \cdots + \frac{x^n}{n!}\]
++++

Recall that to specify a number of digits in your approximation, you may
use:

[latexmath]
++++
\[e_s = \frac{( 0.5 x 10^{2-sigFigs} )}{100} \]
++++

where sigFigs is the minimum number of significant digits in the result.


+*In[ ]:*+
[source, ipython3]
----
#Write implementation here 
def expIter(x):
    sum = 1
    n = 100 
    for i in range(n, 0, -1): 
        sum = 1 + x * sum / i 
        print ("Epsilon =", sum) 
        s
x = 1
expIter(x)
----

The approximation error can be calculated by:

[latexmath]
++++
\[e_{ a } = \frac{current approximation - previous approximation} { current approximation }\]
++++


+*In[ ]:*+
[source, ipython3]
----
# Implement a routine to compare your approximation with the real result, return a Python dictionary.
# The dictionary key should be the number you are approximating, and the values should be given as a list
# with the real value, approximated value, and approximation error. The real value may be computed
# using the function math.exp(). 
----

== 3. Approximate the cubic root of a given positive integer

In this part of the exercise, firstly, we will use a naive approach to
approximate the cubic root of a positive integer. Then, we will use a
simple bisection search, to approximate the same result. Then, we will
count the number of steps taken by both algorithms, and compare them.


+*In[ ]:*+
[source, ipython3]
----
# Write here, the implementation of guess_cube function
# Use a condition to handle the posibility of not guessing the number
def guess_cube(x):
    '''
    Inputs:
        x - Positive integer to guess root  
    
    Outputs:
        cube_guess - Floating point with the cubic root guess
    '''
----


+*In[ ]:*+
[source, ipython3]
----
# Now, implement the same computation, using the simplisting bisection method we saw in class
def bisect_cubic(x):
    '''
    Inputs:
        x - Positive integer to guess root  
    
    Outputs:
        cube_approx - Floating point with the computed cubic root    
    '''
    
----


+*In[ ]:*+
[source, ipython3]
----
# Finally, implement a function that compares the results from both implementations.
# You are free to use any metric for these purposes, i.e. you may report approx error, number of steps or 
# any other performance measurement you consider adecuate.
----


+*In[ ]:*+
[source, ipython3]
----

----
